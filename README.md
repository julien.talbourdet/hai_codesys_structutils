# HAI_CODESYS_StructUtils

## Description
The HAI_CODESYS_StructUtils library contains an abstract function block (FB) called `FB_StructSerializer`, designed to facilitate the serialization and deserialization of structures into key-value format strings.

This FB takes the following parameters:
- `pStruct`: A pointer to the structure to be serialized/deserialized.
- `pString`: A pointer to the string for storing the result.
- `diLength`: The length of the string.
- `xProcessOneByOne`: A boolean indicating whether serialization should be performed line by line.

When `xProcessOneByOne` is enabled, serialization is performed line by line, which can be more memory-efficient for large structures.

## Methods

### `Parse()`
The `Parse()` method allows placing key-value pairs from the string into the structure.

### `Stringify()`
The `Stringify()` method serializes the structure into the string. If `xProcessOneByOne` is `false`, the entire structure is serialized into one string. Otherwise, each method call updates the string with a key-value pair.

The format generated for each key-value pair is: `"Variable_Name:Value$R$N"`.

## Usage

To use this function block:
1. Create a new function block inheriting from `FB_StructSerializer`.
2. Specify the type of the `pStruct` parameter, choosing the type corresponding to the structure to be serialized.
3. Reimplement the abstract method `Build`. Use `HandleVariable` in this method to reference different elements of the structure. For example:

```StructuredText
METHOD Build: BOOL
VAR_INPUT
	pStruct:POINTER TO TYP_EclairageControllerConf;
END_VAR

    HandleVariable(eType:=__SYSTEM.TYPE_CLASS.TYPE_BOOL, sName:='xPlanningGareOutput', pbVar:=ADR(pStruct^.xPlanningGareOutput));
    // Other calls to HandleVariable for other attributes
    RETURN TRUE; // Indicates successful serialization
END_METHOD
```

- `eType` indicates the type of the variable (see the `__SYSTEM.TYPE_CLASS` enumeration in CODESYS).
- `sName` is the name of the attribute to be processed in the structure.
- `pbVar` is a pointer representing the address of the structure's element.